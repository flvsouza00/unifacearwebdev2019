export class Checkin {
    guidCheckin: number;
    token: string;
    guidUsuario: number;
    guidVoo: number;
    guidPassagem: number;
    guidAeronave: number;
    checkinAtivo: boolean;
    altaBagagem: boolean;
    fotoCheckin: string;
    indicadorFoto: boolean;
    guidMilhagem: number;
    dataCheckin: any;
    guidStatus: number;
}